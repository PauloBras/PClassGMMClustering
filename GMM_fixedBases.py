# ------------------------------------------*------------------------------------------
#   Gaussian Mixture Model for unsupervised/semi-supervised learning using LZ data
# ------------------------------------------*------------------------------------------

from math import *

import sys
import datetime
import random
import time
import itertools

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib as mpl
import seaborn as sns

from scipy import linalg
from sklearn import mixture

start_time = time.time()
np.random.seed(0)

filename = 'file.txt'
output_name = 'GMM_fixedBase'

title_name = output_name
date = datetime.datetime.today().strftime('%Y%m%d')

print('---------------------------------------------------------------------------------------------')
print('1. Importing data from: ',filename)
print('---------------------------------------------------------------------------------------------')

data = np.genfromtxt(filename, delimiter='\t', dtype=None, encoding=None, names=('pulseClass', 'pA', 'pF50', 'TBA', 'pH', 'pL', 'pL90', 'aft5', 'aft25', 'aft50', 'aft75', 'aft95', 'pA100', 'pA200', 'pA500', 'pA1k', 'pA2k', 'pA5k', 'pHT', 'pRMSW', 'coincidence', 'rawFileN', 'rqFileN', 'rawEventID', 'rqEventID', 'pulseID'))

print(data.shape)

#----------------------------------------- The pRMSW has some unexpected zeros, which are non-physical.
data['pRMSW'][data['pRMSW']==0]=1

#----------------------------------------- Creating a DataFrame of given dataset.
data=pd.DataFrame({
    'pA':data['pA'],
    'pF50':data['pF50'],
    'pF100':(data['pA100']/data['pA']),
    'pF200':(data['pA200']/data['pA']),
    'pF1k':(data['pA1k']/data['pA']),
    'tba':data['TBA'],
    'pL90':(data['aft95']-data['aft5']),
    'pH':data['pH'],
    'pHTL':(data['pHT']/data['pL']),
    'pRMSW':data['pRMSW'],
    'class':data['pulseClass']
})

#----------------------------------------- Extracting the classes of each pulse and changing Other=0 to Other=4
pulseClass=np.array(data['class'])  # Labels
classes_index = np.unique(pulseClass)
print('Class labels: {}'.format(classes_index))

#----------------------------------------- For the 67 bases example used the following variables: {pA, pF100, TBA, pL90, pH}
data = np.array([np.log10(data['pA']),
    data['pF100'],
    data['tba'],
    np.log10(data['pL90']),
    np.log10(data['pH'])])

data = data.T

print('---------------------------------------------------------------------------------------------')
print('2. Building the GMM model and fitting to the data...')
cv_type = 'full'
n_components = 67
print("   ...considering {} bases, using full covariance".format(n_components))
print('---------------------------------------------------------------------------------------------')

best_gmm = mixture.GaussianMixture(n_components=n_components, covariance_type=cv_type, random_state=32)
best_gmm.fit(data)
lowest_bic = best_gmm.bic(data)

output_name = output_name+'_{}b'.format(n_components)

#-------------------------------------- Predict the populations
results_gmm = best_gmm.predict(data)
results_gmm = np.array(results_gmm)

#----------------------------------------------------------------------------
colors = ['navy', 'turquoise', 'cornflowerblue', 'darkorange',
    'slateblue', 'springgreen', 'darkgoldenrod', 'rosybrown','coral','limegreen',
    'teal','aqua','sienna','olivedrab','dimgray','firebrick','aquamarine','palegreen','crimson','darkkhaki']

colors = np.r_[colors, colors, colors, colors]
mycmap = plt.get_cmap('nipy_spectral')

plt.figure(figsize=(7.5, 6.25))
for h in range(n_components) :
    gauss_1 = plt.scatter(data[results_gmm==h,0],data[results_gmm==h,1],.5, c=mycmap(h/n_components))
plt.xlabel(r'log$_{10}$(pA) [phd]',fontsize=15)
plt.ylabel(r'pF100 [ns]', fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlim(-4.5,7.5)
plt.ylim(-0.1,1.2)
plt.grid()
plt.savefig("{}_results_dense_pApF100_EPJC_spectral.pdf".format(output_name))
plt.savefig("{}_results_dense_pApF100_EPJC_spectral.png".format(output_name),dpi=1200)
plt.clf()

plt.figure(figsize=(7.5, 6.25))
for h in range(n_components) :
    gauss_1 = plt.scatter(data[results_gmm==h,0],data[results_gmm==h,3],.5, c=mycmap(h/n_components))
plt.xlabel(r'log$_{10}$(pA) [phd]',fontsize=15)
plt.ylabel(r'log$_{10}$(pL90) [ns]', fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlim(-4.5,7.5)
plt.ylim(1,6)
plt.grid()
plt.savefig("{}_results_dense_pApL90_EPJC_spectral.pdf".format(output_name))
plt.savefig("{}_results_dense_pApL90_EPJC_spectral.png".format(output_name),dpi=1200)
plt.clf()

plt.figure(figsize=(7.5, 6.25))
for h in range(n_components):
    gauss_1 = plt.scatter(data[results_gmm==h,2],data[results_gmm==h,0],.5, c=mycmap(h/n_components))
plt.xlabel('TBA', fontsize=15)
plt.ylabel(r'log$_{10}$(pA) [phd]',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlim(-1.5,1.5)
plt.ylim(-4.5,7.5)
plt.grid()
plt.savefig("{}_results_dense_TBApA_EPJC_spectral.pdf".format(output_name))
plt.savefig("{}_results_dense_TBApA_EPJC_spectral.png".format(output_name),dpi=1200)
plt.clf()

plt.figure(figsize=(7.5, 6.25))
for h in range(n_components):
    gauss_1 = plt.scatter(data[results_gmm==h,2],data[results_gmm==h,3],.5, c=mycmap(h/n_components))
plt.xlabel('TBA', fontsize=15)
plt.ylabel(r'log$_{10}$(pL90) [phd]',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.xlim(-1.5,1.5)
plt.ylim(1,6)
plt.grid()
plt.savefig("{}_results_dense_TBApL90_EPJC_spectral.pdf".format(output_name))
plt.savefig("{}_results_dense_TBApL90_EPJC_spectral.png".format(output_name),dpi=1200)
plt.clf()

for h in range(n_components):
    plt.figure(figsize=(8, 6))
    gauss_1 = plt.scatter(np.log10(data[results_gmm==h,1]),data[results_gmm==h,0],.5, c=colors[h])
    plt.ylim(-3,7)
    plt.xlim(-5,2)
    plt.ylabel('log10(pA)', fontsize=14)
    plt.xlabel('log10(pF100)', fontsize=14)
    plt.savefig("{}_results_dense_pF100pA_base{}.png".format(output_name,h))
    plt.clf()

    plt.figure(figsize=(8, 6))
    gauss_1 = plt.scatter(data[results_gmm==h,0],data[results_gmm==h,3],.5, c=colors[h])
    plt.ylim(1,6)
    plt.xlim(-3,7)
    plt.ylabel('log10(pL90)', fontsize=14)
    plt.xlabel('log10(pA)', fontsize=14)
    plt.savefig("{}_results_dense_pApL90_base{}.png".format(output_name,h))
    plt.clf()
    
    plt.figure(figsize=(8, 6))
    gauss_1 = plt.scatter(data[results_gmm==h,2],np.log10(data[results_gmm==h,1]),.5, c=colors[h])
    plt.ylim(-5,2)
    plt.xlim(-1.5,1.5)
    plt.ylabel('log10(pF100)', fontsize=14)
    plt.xlabel('TBA', fontsize=14)
    plt.savefig("{}_results_dense_TBApF100_base{}.png".format(output_name,h))
    plt.clf()
    
    plt.figure(figsize=(8, 6))
    gauss_1 = plt.scatter(data[results_gmm==h,2],data[results_gmm==h,0],.5, c=colors[h])
    plt.ylim(-3,7)
    plt.xlim(-1.5,1.5)
    plt.ylabel('log10(pA)', fontsize=14)
    plt.xlabel('TBA', fontsize=14)
    plt.savefig("{}_results_dense_TBApA_base{}.png".format(output_name,h))
    plt.clf()

#----------------------------------------------------------------------------
#-------------------------------------- Confusion matrix
pulseClass_index = pd.Series(pulseClass, name='LZ class')
y_pred_index = pd.Series(results_gmm, name='Predic class')
matConf = pd.crosstab(pd.Series(pulseClass_index), y_pred_index,
        rownames=['LZ class'],
        colnames=['Predic class'],
        margins=False)

matConf_sumClass = np.array(matConf.sum(axis=1))
matConf_sumClass = matConf_sumClass.T
matConf_sumClass = matConf_sumClass.astype(float)

matConf_sumSpecies = np.array(matConf.sum(axis=0))
matConf_sumSpecies = matConf_sumSpecies.astype(float)

matConf_norm = (matConf/matConf_sumSpecies)

matConf_np = np.array(matConf)
matConf_np = matConf_np.astype(float)

matConf_norm_np = np.array(matConf)
matConf_norm_np = matConf_norm_np.astype(float)

matConf_errors = np.copy(matConf_np)
matConf_norm_errors = np.copy(matConf_norm_np)

print('---------------------------------------------------------------------------')
for i in range(len(classes_index)) :
    for j in range(n_components) : 
        matConf_norm_np[i,j] = (matConf_np[i,j])/(matConf_sumSpecies[j])
np.savetxt('MatConf_norm_np.txt',matConf_norm_np)
np.savetxt('MatConf_np.txt',matConf_np)

for i in range(len(classes_index)) :
    for j in range(n_components) : 
        #----------------------------------------------------------
        if matConf_np[i,j] > 0 :
            matConf_errors[i,j] = np.sqrt(matConf_np[i,j])
            matConf_norm_errors[i,j] = (np.sqrt(matConf_np[i,j]))/(matConf_np[i,j])
        else :
            matConf_errors[i,j] = 0.0
            matConf_norm_errors[i,j] = 0.0

np.savetxt('MatConf_errors.txt',matConf_errors)
np.savetxt('MatConf_norm_errors.txt',matConf_norm_errors)

matConf_cum_np = matConf_np.cumsum(axis=0)
matConf_normcum_np = matConf_norm_np.cumsum(axis=0)

plt.figure(figsize=(10, 4))
p1 = plt.bar(range(n_components), matConf_norm_np[classes_index[0],:], .9)
p2 = plt.bar(range(n_components), matConf_norm_np[classes_index[1],:], .9, bottom=matConf_normcum_np[classes_index[0],:])
p3 = plt.bar(range(n_components), matConf_norm_np[classes_index[2],:], .9, bottom=matConf_normcum_np[classes_index[1],:])
p4 = plt.bar(range(n_components), matConf_norm_np[classes_index[3],:], .9, bottom=matConf_normcum_np[classes_index[2],:])
A = range(67)
A = A[::2]
B = [str(i) for i in A]
plt.xticks(A,B)
plt.xlim((-1,78))
plt.legend([p1, p2, p3, p4],['Oth','S1','S2','SE'],loc=1,title='HADES class')
plt.xlabel('GMM base', fontsize=14)
plt.savefig("{}_results_full_speciesClass_norm_2.png".format(output_name))
plt.clf()

#----------------------------------------------------------------------------

print('---------------------------------------------------------------------------------------------')
print("3. Mapping the bases to the average LZ class contained............")
print('---------------------------------------------------------------------------------------------')

Class_Base_mapping = np.around(np.dot(matConf_norm_np.T,[0.0,1.0,2.0,3.0]))

results_gmm_mapped = np.copy(results_gmm)
for base in range(n_components) :
    results_gmm_mapped[results_gmm==base] = Class_Base_mapping[base]

#-------------------------------------- Confusion matrix
pulseClass_index = pd.Series(pulseClass, name='LZ class')
results_index = pd.Series(results_gmm_mapped, name='Predic class')
matConf = pd.crosstab(pd.Series(pulseClass_index), results_index,
        rownames=['LZ class'],
        colnames=['Predic class'],
        margins=False)

for h in classes_index:
    plt.figure(figsize=(8, 6))
    gauss_1 = plt.hist2d(np.log10(data[results_gmm_mapped==h,1]),data[results_gmm_mapped==h,0],bins=(np.linspace(-5.0,2.0,100),np.linspace(-3.0,7.0,100)),cmap=plt.cm.jet, norm=LogNorm())
    plt.ylabel('log10(pA)', fontsize=14)
    plt.xlabel('log10(pF100)', fontsize=14)
    plt.savefig("{}_results_dense_pF100pA_class{}.png".format(output_name,h))
    plt.clf()

    plt.figure(figsize=(8, 6))
    gauss_1 = plt.hist2d(data[results_gmm_mapped==h,0],data[results_gmm_mapped==h,3],bins=(np.linspace(-3.0,7.0,100),np.linspace(1.0,6.0,100)),cmap=plt.cm.jet, norm=LogNorm())
    plt.ylabel('log10(pL90)', fontsize=14)
    plt.xlabel('log10(pA)', fontsize=14)
    plt.savefig("{}_results_dense_pApL90_class{}.png".format(output_name,h))
    plt.clf()
    
    plt.figure(figsize=(8, 6))
    gauss_1 = plt.hist2d(data[results_gmm_mapped==h,2],np.log10(data[results_gmm_mapped==h,1]),bins=(np.linspace(-1.5,1.5,100),np.linspace(-5.0,2.0,100)),cmap=plt.cm.jet, norm=LogNorm())
    plt.ylabel('log10(pF100)', fontsize=14)
    plt.xlabel('TBA', fontsize=14)
    plt.savefig("{}_results_dense_TBApF100_class{}.png".format(output_name,h))
    plt.clf()
    
    plt.figure(figsize=(8, 6))
    gauss_1 = plt.hist2d(data[results_gmm_mapped==h,2],data[results_gmm_mapped==h,0],bins=(np.linspace(-1.5,1.5,100),np.linspace(-3.0,7.0,100)),cmap=plt.cm.jet, norm=LogNorm())
    plt.ylabel('log10(pA)', fontsize=14)
    plt.xlabel('TBA', fontsize=14)
    plt.savefig("{}_results_dense_TBApA_class{}.png".format(output_name,h))
    plt.clf()

print('---------------------------------------------------------------------------------------------')
print("Exporting Bases and new classes...........")
results_file = open("{}_results.txt".format(output_name),"w")
for w in range(len(results_gmm)) :
    results_file.write("{}\t{}\t{}\n".format(results_gmm[w],pulseClass[w],results_gmm_mapped[w]))
results_file.close()
print("DONE!!!!!!")
print('---------------------------------------------------------------------------------------------')



